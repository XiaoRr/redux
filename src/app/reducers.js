import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
export default combineReducers({
  product: productReducer
});